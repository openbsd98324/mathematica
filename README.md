# mathematica

Go to https://user.wolfram.com/. Login or create a Wolfram ID, which is your @universityaccount.com email address. Note: You only need to create this ID once.

Go to https://www.wolfram.com/siteinfo/, enter your @universityaccount.com  email, then choose student or faculty/staff.
